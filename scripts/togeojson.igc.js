toGeoJSON["igc"] = function(doc) {
    var gj = {
        type: 'FeatureCollection',
        features: []
    };
    var coords = [];
    var artime = [];

    igc.initialise(doc);

    for (var i = 0; i < igc.latLong.length; i++) {
	coords.push([igc.latLong[i].lng, igc.latLong[i].lat, igc.pressureAltitude[i]]);
	artime.push(igc.unixStart[0] + igc.recordTime[i]);
    }

    gj.features.push({
	type: 'Feature',
	properties: {
	    time: artime
	},
	geometry: {
            type: 'LineString', //MultiPoint
	    coordinates: coords
	}
    });

    for (t in igc.taskpoints.coords) {
	var tp_name = igc.taskpoints.names[t];
	var tp_lng = igc.taskpoints.coords[t].lng;
	var tp_lat = igc.taskpoints.coords[t].lat;
	gj.features.push({
	    type: 'Feature',
	    properties: {
		name: tp_name
	    },
	    geometry: {
		type: 'Point',
		coordinates: [tp_lng, tp_lat]
	    }
	});
    }

    window.dispatchEvent(new Event('igcloaded'));

    return gj;
};
