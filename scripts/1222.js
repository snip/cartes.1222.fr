var cartes1222 = (function() {
    return {
	map: undefined,
	marker: undefined
    }
})();

(function (window, undefined) {
    'use strict';

    var map;
    var center = null;

    //
    // Fonds de carte
    //

    var tileLayers = [

	// https://umap.openstreetmap.fr/
	{
	    "name": "OSM-Fr",
	    "url_template": "//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png",
	    "minZoom": 0,
	    "maxZoom": 20,
	    "attribution": 'map data \u00a9 <a href="https://osm.org/copyright">OpenStreetMap contributors</a> under ODbL ',
	},
	{
	    "name": "OpenStreetMap",
	    "url_template": "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
	    "minZoom": 0,
	    "maxZoom": 19,
	    "attribution": 'map data \u00a9 <a href="https://osm.org/copyright">OpenStreetMap contributors</a> under ODbL ',
	},
	{
	    "name": "OSM Humanitarian (OSM-FR)",
	    "url_template": "//{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
	    "minZoom": 0,
	    "maxZoom": 20,
	    "attribution": 'map data \u00a9 <a href="https://osm.org/copyright">OpenStreetMap contributors</a> under ODbL  - Tiles \u00a9 HOT',
	},
	{
	    "name": "OSM Landscape (Thunderforest)",
	    "url_template": "https://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey=e6b144cfc47a48fd928dad578eb026a6",
	    "minZoom": 0,
	    "maxZoom": 18,
	    "attribution": 'Tiles \u00a9 <a href="https://www.thunderforest.com/maps/outdoors/">Gravitystorm</a> / map data \u00a9 <a href="https://osm.org/copyright">OpenStreetMap contributors</a> under ODbL',
	},
	{
	    "name": "OSM Transport (Thunderforest)",
	    "url_template": "https://{s}.tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=e6b144cfc47a48fd928dad578eb026a6",
	    "minZoom": 0,
	    "maxZoom": 18,
	    "attribution": 'Tiles \u00a9 <a href="https://www.thunderforest.com/maps/outdoors/">Gravitystorm</a> / map data \u00a9 <a href="https://osm.org/copyright">OpenStreetMap contributors</a> under ODbL',
	},

	// https://www.geoportail.gouv.fr/donnees/carte-oaci-vfr
	{
	    "name": "OACI-VFR (IGN/SIA)",
	    "url_template": "https://wxs.ign.fr/an7nvfzojv5wa96dsga5nk8w/geoportail/wmts?layer=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-OACI&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fjpeg&TileMatrix={z}&TileCol={x}&TileRow={y}",
	    "minZoom": 6,
	    "maxZoom": 11,
	    "attribution": '<a href="https://www.geoportail.gouv.fr/depot/api/cgu/CGU_API_libre.pdf"><img src="https://wxs.ign.fr/static/logos/IGN/IGN.gif" alt="IGN" height="32" /></a>',
	},
	{
	    "name": "IGN Image a\u00e9rienne (France)",
	    "url_template": "https://wxs.ign.fr/an7nvfzojv5wa96dsga5nk8w/geoportail/wmts?layer=ORTHOIMAGERY.ORTHOPHOTOS&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fjpeg&TileMatrix={z}&TileCol={x}&TileRow={y}",
	    "minZoom": 0,
	    "maxZoom": 19,
	    "attribution": '<a href="https://www.geoportail.gouv.fr/depot/api/cgu/CGU_API_libre.pdf"><img src="https://wxs.ign.fr/static/logos/IGN/IGN.gif" alt="IGN" height="32" /></a>',
	},
	{
	    "name": "Carte IGN",
	    "url_template": "https://wxs.ign.fr/an7nvfzojv5wa96dsga5nk8w/geoportail/wmts?layer=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/jpeg&TileMatrix={z}&TileCol={x}&TileRow={y}",
	    "minZoom": 0,
	    "maxZoom": 18,
	    "attribution": '<a href="https://www.geoportail.gouv.fr/carte"><img src="https://wxs.ign.fr/static/logos/IGN/IGN.gif" alt="IGN" height="32" /></a>'
	}
    ];

    var baseMaps = {};
    for (var i=0 ; i < tileLayers.length ; i++) {
	baseMaps[tileLayers[i].name] = L.tileLayer((tileLayers[i].url_template.substr(0, 2)=="//") ? "https:" + tileLayers[i].url_template : tileLayers[i].url_template, {
	    minZoom: tileLayers[i].minZoom,
	    maxZoom: tileLayers[i].maxZoom,
	    attribution: tileLayers[i].attribution,
	});
    }

    //
    // Couches
    //
    var openaip_airspaces = L.tileLayer("https://{s}.tile.maps.openaip.net/geowebcache/service/tms/1.0.0/openaip_basemap@EPSG%3A900913@png/{z}/{x}/{y}.png", {
	attribution: 'Espaces aériens <a href="https://openaip.net">OpenAIP</a>',
	maxZoom: 14,
	minZoom: 4,
	tms: true,
	detectRetina: true,
	subdomains: '12',
	format: 'image/png',
	transparent: true,
    });

    var openaip_airports = L.tileLayer("https://{s}.tile.maps.openaip.net/geowebcache/service/tms/1.0.0/openaip_approved_airports@png/{z}/{x}/{y}.png", {
	attribution: 'Aérodromes <a href="https://openaip.net">OpenAIP</a>',
	maxZoom: 14,
	minZoom: 4,
	tms: true,
	detectRetina: true,
	subdomains: '1',
	format: 'image/png',
	transparent: true,
    });

    var overlayMaps = {
	"Espaces aériens (OpenAIP)": openaip_airspaces,
	"Aérodromes (OpenAIP)": openaip_airports
    };


    function initLeafletFileLayer() {
	var L = window.L;
	var style = {
	    color: 'red',
	    opacity: 1.0,
	    fillOpacity: 1.0,
	    weight: 2,
	    clickable: false
	};
	L.Control.FileLayerLoad.LABEL = '<img class="icon" src="libraries/Leaflet.FileLayer/folder.svg" alt="file"/>';
	return L.Control.fileLayerLoad({
	    fitBounds: true,
	    layerOptions: {
		style: style,
		pointToLayer: function (data, latlng) {
		    var m = L.marker(latlng);
		    if (data.properties && data.properties.name) {
			m.bindPopup(data.properties.name);
		    }
		    return m;
		}
	    }
	});
    }

    function generateCircles(pos, desc, km) {
	var circles = [];

	for (var i=0; i<km.length; ++i) {
	    circles[i] = L.circle(pos, km[i]*1000, {
		color: ((km[i]>=100 || km[i]<10) ? 'red' : 'black'),
		fillOpacity: 0,
		weight: (km[i]<100 && km[i] % 10 == 0) ? 2 : 1
	    });
	}
	circles[circles.length] = L.marker(pos).bindPopup(desc).openPopup();

	return L.layerGroup(circles);
    }

    function initMap() {
	var L = window.L;
	//
	// Leaflet Map
	//
	map = L.map('carte', {
	    center: [46.449,2.210],
	    zoom: 6,
	    layers: [baseMaps["OSM-Fr"]]
	});
	cartes1222.map = map;
	L.control.scale({'imperial': false}).addTo(map);
	var control_layers = L.control.layers(baseMaps, overlayMaps);
	control_layers.addTo(map);

	var control = initLeafletFileLayer();
	control.addTo(map);
	control.loader.on('data:loaded', function (e) {
	    var layer = e.layer;
	    console.log(layer);
	});

	// leaflet-ruler
	var leaflet_ruler_options = {
	    position: 'topleft',            // Leaflet control position option
	    circleMarker: {                 // Leaflet circle marker options for points used in this plugin
		color: 'red',
		radius: 2
	    },
	    lineStyle: {                    // Leaflet polyline options for lines used in this plugin
		color: 'red',
		dashArray: '1,6'
	    },
	    lengthUnit: {                   // You can use custom length units. Default unit is kilometers.
		display: 'km',              // This is the display value will be shown on the screen. Example: 'meters'
		decimal: 2,                 // Distance result will be fixed to this value.
		factor: null,               // This value will be used to convert from kilometers. Example: 1000 (from kilometers to meters)
		label: 'Distance :'
	    },
	    angleUnit: {
		display: '&deg;',           // This is the display value will be shown on the screen. Example: 'Gradian'
		decimal: 2,                 // Bearing result will be fixed to this value.
		factor: null,               // This option is required to customize angle unit. Specify solid angle value for angle unit. Example: 400 (for gradian).
		label: 'Orientation :'
	    }
	}
	L.control.ruler(leaflet_ruler_options).addTo(map);

	// Gestion des paramètres
	var parsedUrl = new URL(window.location.href);
	var layersParam = parsedUrl.searchParams.get('layers');
	var centerParam = parsedUrl.searchParams.get('center');
	var flightParam = parsedUrl.searchParams.get('flight');
	var selectedLayersParam = parsedUrl.searchParams.get('l');
	var baseMapParam = parsedUrl.searchParams.get('b');

	// Raccourci "aacm"
	if (centerParam == null && layersParam == null) {
	    var aacmParam = parsedUrl.searchParams.get('aacm');
	    if (aacmParam != null) {
		layersParam = "./aacm/layers.json"
		centerParam = "./aacm/center.json"
	    }
	}

	// Paramètre "center"
	if (centerParam != null) {
	    fetch(centerParam).then(function(response) {
		return response.json();
	    }).then(function(json) {
		console.log(json);
		map.setView(new L.LatLng(json.lat, json.lng), 11);
		if (json.oaci != "") {
		    var lfpk_circles = generateCircles([json.lat, json.lng], json.oaci + " " + json.name, [5, 10, 15, 20, 25, 30, 100, 200]);
		    control_layers.addOverlay(lfpk_circles, "Cercles " + json.oaci);
		    lfpk_circles.addTo(map);
		    center = json;
		}
	    });
	}

	// Paramètre "layers"
	if (layersParam != null) {
	    fetch(layersParam).then(function(response) {
		return response.json();
	    }).then(function(json) {
		console.log(json);
		var myStyle = {
		    "color": "#424242",
		    "weight": 5,
		    "opacity": 1
		};
		var L_GeoJSON_AJAX_params = {
		    style: function (feature) {
			var s = myStyle;
			if (feature.properties && feature.properties.color) {
			    s.color = feature.properties.color;
			}
			return s;
		    },
		    pointToLayer: function (feature, latlng) {
			return L.marker(latlng);
		    },
		    onEachFeature: function (feature, layer) {
			if (feature.properties && feature.properties.name) {
			    layer.bindPopup(feature.properties.name);
			}
		    }
		};
		for (var overlay_title in json.overlays) {
		    if (typeof json.overlays[overlay_title] === 'object' && "type" in json.overlays[overlay_title]) {
			if (json.overlays[overlay_title]["type"] == "L.tileLayer") {
			    var overlay = L.tileLayer(json.overlays[overlay_title]["url"], json.overlays[overlay_title]["params"]);
			} else if (json.overlays[overlay_title]["type"] == "L.GeoJSON.AJAX") {
			    var overlay = new L.GeoJSON.AJAX(json.overlays[overlay_title]["url"], L_GeoJSON_AJAX_params);
			}
		    } else {
			var overlay = new L.GeoJSON.AJAX(json.overlays[overlay_title], L_GeoJSON_AJAX_params);
		    }
		    control_layers.addOverlay(overlay, overlay_title);
		}
	    });
	}

	// Paramètre "l" - selectedLayers
	overlayMaps["Aérodromes (OpenAIP)"].addTo(map); // Activer les layers à ajouter par défaut
	if (selectedLayersParam != null) { // Uniquement si le paramètre selectedLayers est utilisé :
	    // Désactiver l'ensemble des layers
	    for (var o in overlayMaps) {
		overlayMaps[o].remove(map);
	    }

	    // Activer uniquement les layers mentionnés
	    var selectedLayers = selectedLayersParam.split('|');
	    for (var n in selectedLayers) {
		var l = selectedLayers[n];
		if (l in overlayMaps)
		    overlayMaps[l].addTo(map);
	    }
	}

	// Paramètre "b" - baseMap
	if (baseMapParam != null && baseMapParam in baseMaps) {
	    baseMaps[baseMapParam].addTo(map)
	}

	// Paramètre "flight"
	if (flightParam != null) {
	    fetch('proxy/proxy.php?url='+flightParam).then(function(response) {
		return response.text();
	    }).then(function(text) {
		var content = toGeoJSON.igc(text);
		var layer = L.FileLayer.FileLoader.prototype.options.layer(content, control.options.layerOptions);
		layer.addTo(map);
		control.loader.fire('data:loaded', {layer: layer});
	    });
	}

	// En l'absence des paramètres "center" ou "flight", demande de géolocalisation
	if (centerParam == null && flightParam == null) {
	    map.locate({setView: true, maxZoom: 11});
	}
    }

    function buildGraph() {
	var time_records = [];
	var gps_records = [];
	var baro_records = [];
	var dist = [];
	var finesse10_records = [];
	var finesse20_records = [];
	var finesse25_records = [];
	var fomatDate = function(d) {
	    var YYYY = d.getFullYear();
	    var MM = d.getMonth()+1; if (MM < 10) MM = "0" + MM;
	    var DD = d.getDate(); if (DD < 10) DD = "0" + DD;
	    var hh = d.getHours(); if (hh < 10) hh = "0" + hh;
	    var mm = d.getMinutes(); if (mm < 10) mm = "0" + mm;
	    var ss = d.getSeconds(); if (ss < 10) ss = "0" + ss;
	    return YYYY+"-"+MM+"-"+DD+" "+hh+":"+mm+":"+ss;
	}

	/* Préparation des données */
	var vol_campagne = false;
	var vz_records = [];
	var gspeed = igc.groundSpeed.map(x => Math.round(x));
	gspeed.unshift(0);
	var tmp_altitude_gps_modif = false;
	var tmp_altitude_bar_modif = false;
	var tmp_altitude_diff = false;
	for (var i = 0; i < igc.recordTime.length; i++) {
	    // Temps
	    var tmpd = new Date((igc.unixStart[0] + igc.recordTime[i] - igc.recordTime[0])*1000);
	    time_records[i] = fomatDate(tmpd);

	    // Altitude
	    if (!tmp_altitude_gps_modif && i > 0 && igc.gpsAltitude[i] != igc.gpsAltitude[i-1])
		tmp_altitude_gps_modif = true;
	    if (!tmp_altitude_bar_modif && i > 0 && igc.pressureAltitude[i] != igc.pressureAltitude[i-1])
		tmp_altitude_bar_modif = true;
	    if (!tmp_altitude_diff && igc.gpsAltitude[i] != igc.pressureAltitude[i])
		tmp_altitude_diff = true;
	    gps_records[i] = igc.gpsAltitude[i];
	    baro_records[i] = igc.pressureAltitude[i];

	    // Vitesse verticale
	    vz_records.push(Math.round(igc.getClimb(i)*10)/10);

	    // Finesse
	    if (center != null && !vol_campagne) {
		dist[i] = L.latLng([center.lat, center.lng]).distanceTo(L.latLng([igc.latLong[i].lat, igc.latLong[i].lng]));
		if (dist[i] >= 5000) {
		    finesse10_records[i] = Math.ceil(dist[i]/10) + center.reserve_finesse['10'];
		    if (dist[i] >= 10000) {
			finesse20_records[i] = Math.ceil(dist[i]/20) + center.reserve_finesse['20'];
			finesse25_records[i] = Math.ceil(dist[i]/25) + center.reserve_finesse['25'];
			if (baro_records[i] > 0 && finesse25_records[i] > 2*baro_records[i] ||
			    gps_records[i] > 0 && finesse25_records[i] > 2*gps_records[i]) {
			    vol_campagne = true;
			}
		    }
		}
	    }
	}

	// Altitude
	var data = [];
	if (!tmp_altitude_diff) {
	    data.push({
		x: time_records,
		y: gps_records,
		type: 'scatter',
		name: 'Altitude (m)'
	    });
	} else if (tmp_altitude_bar_modif) {
	    data.push({
		x: time_records,
		y: baro_records,
		type: 'scatter',
		name: 'Altitude barométrique (m)'
	    });
	} else {
	    data.push({
		x: time_records,
		y: gps_records,
		type: 'scatter',
		name: 'Altitude GPS (m)'
	    });
	}

	// Vitesses
	/*
	data.push({
	    x: time_records,
	    y: gspeed,
	    type: 'scatter',
	    name: 'Vitesse sol (km/h)'
	});
	data.push({
	    x: time_records,
	    y: vz_records,
	    type: 'scatter',
	    name: 'Vitesse verticale (m/s)'
	});
	*/

	// Finesse
	if (center != null && !vol_campagne) {
	    data.push({
		x: time_records,
		y: finesse10_records,
		type: 'scatter',
		name: 'Finesse 10'
	    });
	    data.push({
		x: time_records,
		y: finesse20_records,
		type: 'scatter',
		name: 'Finesse 20'
	    });
	    data.push({
		x: time_records,
		y: finesse25_records,
		type: 'scatter',
		name: 'Finesse 25'
	    });
	}

	var layout = {
	    showlegend: true,
	    autosize: true,
	    xaxis: {fixedrange: true, showticklabels: true},
	    yaxis: {fixedrange: true, showticklabels: true},
	    margin: {
		l: 40,
		r: 40,
		b: 40,
		t: 5,
		pad: 0
	    }
	};
	if (window.screen.availWidth <= 960) {
	    layout.showlegend = false;
	    layout.yaxis.showticklabels = false;
	    layout.margin.l = 5;
	    layout.margin.r = 5;
	}
	if (window.screen.availHeight <= 700) {
	    layout.xaxis.showticklabels = false;
	    layout.margin.b = 30;
	    document.getElementById('graph').style.height = '75px';
	    document.getElementById('vitesses').style.top = '87px';
	}

	/* Rendu du graphique */
	var myPlot = document.getElementById('graph');
	myPlot.style.visibility = 'hidden';
	myPlot.style.display = 'block';
	Plotly.newPlot('graph', data, layout, {displayModeBar: false, responsive: true});
	myPlot.style.visibility = 'visible';

	document.getElementById('vitesses').style.display = 'block';

	/* Interaction avec la carte */
	var planeurIcon = L.icon({
	    iconUrl: 'images/planeur.png',
	    iconSize: [32, 32]
	});
	cartes1222.marker = new L.marker(new L.LatLng(igc.latLong[0].lat, igc.latLong[0].lng), {icon: planeurIcon, rotationAngle: 0, rotationOrigin: 'center center'}).addTo(map);
	myPlot.on('plotly_hover', function(data){
	    var i = data.points[0].pointIndex;
	    cartes1222.marker.setLatLng(new L.LatLng(igc.latLong[i].lat, igc.latLong[i].lng));
	    var bearing = utils.toPoint(igc.latLong[(i <= 0) ? 0 : i-1], igc.latLong[i]).bearing;
	    cartes1222.marker.setRotationAngle(bearing);
	    document.getElementById('vitesse_sol').innerText = Math.round(igc.groundSpeed[i <= 0 ? 0 : i-1]);
	    document.getElementById('vitesse_verticale').innerText = Math.round(igc.getClimb(i-1)*10)/10;
	});
	myPlot.on('plotly_click', function(data){
	    var i = data.points[0].pointIndex;
	    cartes1222.marker.setLatLng(new L.LatLng(igc.latLong[i].lat, igc.latLong[i].lng));
	    map.panTo(new L.LatLng(igc.latLong[i].lat, igc.latLong[i].lng));
	    document.getElementById('vitesse_sol').innerText = Math.round(igc.groundSpeed[i-1]);
	    document.getElementById('vitesse_verticale').innerText = Math.round(igc.getClimb(i-1)*10)/10;
	});
    }

    function buildInfos() {
	var infos_arr = [];
	for (var h in igc.headers) {
	    infos_arr.push(igc.headers[h].name + '&nbsp;: ' + igc.headers[h].value);
	}
	document.getElementById('infos').innerHTML = infos_arr.join('<br />');
	if (window.screen.availWidth > 960) {
	    document.getElementById('infos').style.display = 'block';
	}
    }

    window.addEventListener('load', function () {
	initMap();
    });

    window.addEventListener('igcloaded', function () {
	buildGraph();
	buildInfos();
	L.control.igcDetails({position: 'topleft'}).addTo(map);
    });

}(window));


(function(factory, window){
  "use strict";
  if (typeof define === 'function' && define.amd) {
    define(['leaflet'], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require('leaflet'));
  }
  if (typeof window !== 'undefined' && window.L) {
    window.L.IgcDetails = factory(L);
  }
}(function (L) {
  "use strict";
    L.Control.IgcDetails = L.Control.extend({
	_state_graph: true,
	_state_infos: true,
	_state_ascendances: false,
	onAdd: function(map) {
	    this._buildAscendancesGraph();
	    this._map = map;
	    this._container = L.DomUtil.create('div', 'leaflet-bar leaflet-igcdetails');
	    L.DomEvent.disableClickPropagation(this._container);

	    this._link_g = L.DomUtil.create('a', 'leaflet-igcdetails-link', this._container);
	    this._link_g.innerText = 'G';
	    L.DomEvent.on(this._link_g, 'click', this._toggleGraph, this);
	    if (this._state_graph) this._link_g.classList.add("active");

	    this._link_i = L.DomUtil.create('a', 'leaflet-igcdetails-link', this._container);
	    this._link_i.innerText = 'I';
	    L.DomEvent.on(this._link_i, 'click', this._toggleInfos, this);
	    if (this._state_infos) this._link_i.classList.add("active");

	    this._link_a = L.DomUtil.create('a', 'leaflet-igcdetails-link', this._container);
	    this._link_a.innerText = 'A';
	    L.DomEvent.on(this._link_a, 'click', this._toggleAscendances, this);
	    if (this._state_ascendances) this._link_a.classList.add("active");

	    return this._container;
	},
	onRemove: function() {
	    L.DomEvent.off(this._container, 'click', this._toggleMeasure, this);
	},
	_toggleGraph: function() {
	    this._state_graph = !this._state_graph;
	    if (this._state_graph) {
		window.document.getElementById('graph').style.display = 'block';
		window.document.getElementById('vitesses').style.display = 'block';
		this._link_g.classList.add("active");
	    }
	    else {
		window.document.getElementById('graph').style.display = 'none';
		window.document.getElementById('vitesses').style.display = 'none';
		this._link_g.classList.remove("active");
	    }
	},
	_toggleInfos: function() {
	    this._state_infos = !this._state_infos;
	    if (this._state_infos) {
		window.document.getElementById('infos').style.display = 'block';
		this._link_i.classList.add("active");
	    }
	    else {
		window.document.getElementById('infos').style.display = 'none';
		this._link_i.classList.remove("active");
	    }
	},
	_toggleAscendances: function() {
	    this._state_ascendances = !this._state_ascendances;
	    if (this._state_ascendances) {
		window.document.getElementById('ascendances').style.display = 'block';
		this._link_a.classList.add("active");
	    }
	    else {
		window.document.getElementById('ascendances').style.display = 'none';
		this._link_a.classList.remove("active");
	    }
	},
	_buildAscendancesGraph: function() {
	    var ascendances = [];
	    var asc_start = false;
	    var asc_end = false;
	    var asc_sens = "";
	    var asc_sum = 0;
	    var j = 0;
	    var tmp_altitude_bar_modif = false;

	    for (var i in igc.turnRate) {
		// Altitude
		if (!tmp_altitude_bar_modif && i > 0 && igc.pressureAltitude[i] != igc.pressureAltitude[i-1])
		    tmp_altitude_bar_modif = true;

		if (i < 2) continue;

		if (asc_start == false) {
		    if ((igc.turnRate[i] > +3 && igc.turnRate[i-1] > +3 && igc.turnRate[i-2] > +3) ||
			(igc.turnRate[i] < -3 && igc.turnRate[i-1] < -3 && igc.turnRate[i-2] < -3)) {
			asc_start = i-2;
			asc_sens = (igc.turnRate[i] > 0) ? "right" : "left";
		    }
		}
		if (asc_start != false && asc_end == false) {
		    if (Math.abs(igc.turnRate[i]) < 3 && Math.abs(igc.turnRate[i-1]) < 3 && Math.abs(igc.turnRate[i-2]) < 3) {
			asc_end = i-2;
			asc_sum = 0;
			var j = asc_start;
			while (j <= asc_end) {
			    asc_sum += igc.turnRate[j];
			    j++;
			}
			if (Math.abs(asc_sum) >= 180) {
			    var alt_start = (tmp_altitude_bar_modif) ? igc.pressureAltitude[asc_start] : igc.gpsAltitude[asc_start];
			    var alt_end = (tmp_altitude_bar_modif) ? igc.pressureAltitude[asc_end] : igc.gpsAltitude[asc_end];
			    var t = {
				"lat": igc.latLong[asc_start].lat,
				"lng": igc.latLong[asc_start].lng,
				//"start": asc_start,
				//"end": asc_end,
				"alt_start": alt_start,
				"alt_end": alt_end,
				//"time_start": igc.recordTime[asc_start],
				//"time_end": igc.recordTime[asc_end],
				"duration": igc.recordTime[asc_end] - igc.recordTime[asc_start],
				"gain": alt_end - alt_start,
				"sens": asc_sens,
				"sum": asc_sum,
				"avg": (alt_end - alt_start)*1.0/(igc.recordTime[asc_end] - igc.recordTime[asc_start])
			    };
			    ascendances.push(t);
			}
			asc_end = false;
			asc_start = false;
			asc_sens = "";
			asc_sum = 0;
		    }
		}
	    }

	    var x_coords = [];
	    var y_coords = [];
	    var s_coords = [];
	    var t_coords = [];
	    var c_coords = [];

	    for (var a in ascendances) {
		if (ascendances[a].gain > 0 || true) {
		    x_coords.push(Math.round(ascendances[a].avg*100)/100);
		    y_coords.push(ascendances[a].alt_start);
		    s_coords.push(Math.abs(ascendances[a].gain)/10.0);
		    t_coords.push("Gain : " + ascendances[a].gain + "m<br />Altitude de départ : " + ascendances[a].alt_start + "<br />Sens de spirale : " + ((ascendances[a].sens == "right") ? "droite" : "gauche"));
		    c_coords.push((ascendances[a].sens == "right") ? "rgb(93, 164, 214)" : "rgb(255, 144, 14)");
		}
	    }

	    var trace1 = {
		x: x_coords,
		y: y_coords,
		text: t_coords,
		mode: 'markers',
		marker: {
		    color: c_coords,
		    size: s_coords
		}
	    };

	    var data = [trace1];

	    var layout = {
		title: 'Marker Size',
		showlegend: false,
		height: 600,
		width: 600
	    };
	    var layout = {
		showlegend: false,
		autosize: true,
		xaxis: {fixedrange: true, showticklabels: true, "showspikes": true},
		yaxis: {fixedrange: true, showticklabels: true, "showspikes": true},
		margin: {
		    l: 40,
		    r: 40,
		    b: 40,
		    t: 5,
		    pad: 0
		},
	    };


	    var ascendancesPlot = document.getElementById('ascendances');
	    Plotly.newPlot('ascendances', data, layout, {displayModeBar: false, responsive: true});

	    ascendancesPlot.on('plotly_click', function(data){
		var i = data.points[0].pointIndex;
		cartes1222.marker.setLatLng(new L.LatLng(ascendances[i].lat, ascendances[i].lng));
		cartes1222.map.panTo(new L.LatLng(ascendances[i].lat, ascendances[i].lng));
	    });
	}
  });
  L.control.igcDetails = function(options) {
    return new L.Control.IgcDetails(options);
  };
}, window));
